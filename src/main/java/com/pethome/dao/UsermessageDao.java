package com.pethome.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pethome.entity.Usermessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * (Usermessage)表数据库访问层
 *
 * @author wangmingjie
 * @since 2023-04-25 14:25:44
 */
public interface UsermessageDao extends BaseMapper<Usermessage> {

    /**
     * 用户登录(账号密码)
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    @Select("select count(1) from usermessage " +
            "where (user_account = #{userAccount} OR user_phone = #{userAccount}) " +
            "and user_password = #{userPassword} ")
    Integer userLoginForAccount(Usermessage usermessage);

    /**
     * 查询是否有手机号
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    @Select("select count(1) from usermessage " +
            "where user_phone = #{userPhone} ")
    Integer userLoginForPhone(Usermessage usermessage);


    /**
     * 查询有没有注册此账号
     *
     * @param userAccount 用户帐户
     * @return {@link Integer}
     */
    @Select("select count(1) from usermessage " +
            "where user_account = #{userAccount} ")
    Integer selectCountAccount(@Param("userAccount")String userAccount);

    /**
     * 注册用户
     *
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    @Insert("INSERT INTO usermessage (user_account,user_name,user_password,user_phone) " +
            "VALUES (#{userAccount},#{userName},#{userPassword},#{userPhone})")
    Integer regiterUser(Usermessage usermessage);

    /**
     * 更新用户密码
     *
     * @param userPassword 用户密码
     * @param userPhone    用户电话
     * @return {@link Integer}
     */
    @Update("UPDATE usermessage SET user_password = #{userPassword} " +
            "WHERE user_phone = #{userPhone}")
    Integer updateUserPassword(@Param("userPassword")String userPassword,@Param("userPhone")String userPhone);

    /**
     * 查询用户信息通过账号名
     *
     * @param userAccount 用户帐户
     * @return {@link Usermessage}
     */
    @Select("SELECT u.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_password AS userPassword ," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_role AS userRole," +
            "u.user_sex AS userSex," +
            "u.user_email AS userEmail," +
            "u.user_phone AS userPhone," +
            "u.user_address AS userAddress," +
            "u.user_birthday AS userBirthday," +
            "u.user_introduction AS userIntroduction, " +
            "c.file_path AS filePath " +
            "FROM usermessage u inner join cim_img_file c " +
            "on u.user_header = c.id WHERE (u.user_account = #{userAccount} OR u.user_phone = #{userAccount})")
    Usermessage selectUserMessageByAccount(@Param("userAccount")String userAccount);


    /**
     * 查询用户信息通过电话
     *
     * @param userPhone 用户电话
     * @return {@link Usermessage}
     */
    @Select("SELECT u.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_password AS userPassword ," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_role AS userRole," +
            "u.user_sex AS userSex," +
            "u.user_email AS userEmail," +
            "u.user_phone AS userPhone," +
            "u.user_address AS userAddress," +
            "u.user_birthday AS userBirthday," +
            "u.user_introduction AS userIntroduction, " +
            "c.file_path AS filePath " +
            "FROM usermessage u inner join cim_img_file c " +
            "on u.user_header = c.id WHERE u.user_phone = #{userPhone})")
    Usermessage selectUserMessageByPhone(@Param("userPhone")String userPhone);


    /**
     * 查找粉丝数
     *
     * @param userId 用户id
     * @return {@link Integer}
     */
    @Select("SELECT COUNT(1) FROM relation WHERE relation_to_id = #{userId} AND relation_deleted = 1")
    Integer getFansCount(@Param("userId")Integer userId);


    /**
     * 互相关注的数量
     *
     * @param userId 用户id
     * @return {@link Integer}
     */
    @Select("SELECT count(1) FROM relation WHERE user_id = #{userId} " +
            "AND relation_to_id IN ( SELECT user_id FROM relation where relation_to_id = #{userId}) " +
            "AND relation_deleted = 1")
    Integer getFollowFansCount(@Param("userId")Integer userId);



    Usermessage selectUserMessByUserId(@Param("userId")Integer userId);

}

