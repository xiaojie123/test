package com.pethome.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pethome.entity.CimImgFile;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * cim图片文件(CimImgFile)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-23 09:33:45
 */
public interface CimImgFileDao extends BaseMapper<CimImgFile> {

    @Select("select id AS id," +
            "deleted AS deleted," +
            "file_name AS fileName," +
            "suffix AS suffix," +
            "file_size AS fileSize," +
            "crc32 AS crc32," +
            "md5 AS md5," +
            "file_path AS filePath," +
            "create_time AS createTime " +
            "FROM cim_img_file " +
            "where crc32 = #{crc32}")
    List<CimImgFile> getImgFileByCrc32(long crc32);

    @Insert("INSERT INTO cim_img_file " +
            "(file_name,suffix,file_size,crc32,md5,file_path,create_time) " +
            "VALUES (#{cimImgFile.fileName},#{cimImgFile.suffix},#{cimImgFile.fileSize},#{cimImgFile.crc32},#{cimImgFile.md5},#{cimImgFile.filePath},#{cimImgFile.createTime})")
    Integer insertUploadImage(@Param("cimImgFile")CimImgFile cimImgFile);

}

