package com.pethome.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pethome.entity.Trends;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * (Trends)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-21 19:50:53
 */
public interface TrendsDao extends BaseMapper<Trends> {


    /**
     * 查询所有动态
     *
     * @return {@link List}<{@link Trends}>
     */
    @Select("SELECT t.trends_id AS trendsId," +
            "t.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_address AS userAddress," +
            "c.file_path AS filePath," +
            "t.trends_title AS trendsTitle," +
            "t.trends_createtime AS trendsCreatetime," +
            "t.trends_content AS trendsContent," +
            "(SELECT COUNT(1) FROM trends_good WHERE trends_id = t.trends_id AND good_deleted = 1) AS goodCount," +
            "(SELECT COUNT(1) FROM trends_comment WHERE trends_id = t.trends_id AND comment_deleted = 1) AS commentCount," +
            "t.trends_photo AS trendsPhoto FROM" +
            "`trends` t INNER JOIN usermessage u ON t.user_id = u.user_id inner join cim_img_file c " +
            "on c.id = u.user_header WHERE t.trends_deleted = 1 ORDER BY t.trends_createtime DESC")
    List<Trends> selectAllTrendsMessage();


    /**
     * 查询所有动态通过userId
     *
     * @return {@link List}<{@link Trends}>
     */
    @Select("SELECT t.trends_id AS trendsId," +
            "t.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_address AS userAddress," +
            "c.file_path AS filePath," +
            "t.trends_title AS trendsTitle," +
            "t.trends_createtime AS trendsCreatetime," +
            "t.trends_content AS trendsContent," +
            "(SELECT COUNT(1) FROM trends_good WHERE trends_id = t.trends_id AND good_deleted = 1) AS goodCount," +
            "(SELECT COUNT(1) FROM trends_comment WHERE trends_id = t.trends_id AND comment_deleted = 1) AS commentCount," +
            "(SELECT COUNT(1) FROM trends_good WHERE user_id = #{userId} AND trends_id = t.trends_id AND good_deleted = 1) AS goodRed," +
            "t.trends_photo AS trendsPhoto FROM" +
            "`trends` t INNER JOIN usermessage u ON t.user_id = u.user_id inner join cim_img_file c " +
            "on c.id = u.user_header WHERE t.trends_deleted = 1 ORDER BY t.trends_createtime DESC")
    List<Trends> selectAllTrendsMessageByUserId(@Param("userId")Integer userId);

    /**
     * 查询热门动态
     *
     * @return {@link List}<{@link Trends}>
     */
    @Select("SELECT trends_id AS trendsId ," +
            "trends_title AS trendsTitle , " +
            "( SELECT COUNT( 1 ) FROM trends_good WHERE trends_id = t.trends_id AND good_deleted = 1) AS goodCount " +
            "FROM `trends` t WHERE " +
            "t.trends_deleted = 1 ORDER BY " +
            "goodCount DESC LIMIT 10")
    List<Trends> selectHotTrend();


    /**
     * 判断有没有点赞
     * @param trendsId
     * @param userId
     * @return {@link Integer}
     */
    @Select("SELECT COUNT(1) FROM `trends_good` WHERE trends_id = #{trendsId} AND user_id = #{userId} AND good_deleted = 1")
    Integer selectGoodTrends(@Param("trendsId")Integer trendsId,@Param("userId")Integer userId);

    /**
     * 新增点赞
     * @param trendsId
     * @param userId
     * @return {@link Integer}
     */
    @Insert("INSERT INTO trends_good (trends_id,user_id) VALUES (#{trendsId},#{userId})")
    Integer InsertGoodTrends(@Param("trendsId")Integer trendsId,@Param("userId")Integer userId);

    /**
     * 取消点赞
     * @param trendsId
     * @param userId
     * @return {@link Integer}
     */
    @Update("UPDATE trends_good SET good_deleted = 0 WHERE trends_id = #{trendsId} AND user_id = #{userId}")
    Integer removeGoodTrends(@Param("trendsId")Integer trendsId,@Param("userId")Integer userId);


    /**
     * 登录前根据动态id查询动态信息
     *
     * @param trendsId 趋势id
     * @return {@link Trends}
     */
    @Select("SELECT t.trends_id AS trendsId," +
            "t.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_address AS userAddress," +
            "c.file_path AS filePath," +
            "t.trends_title AS trendsTitle," +
            "t.trends_createtime AS trendsCreatetime," +
            "t.trends_content AS trendsContent," +
            "(SELECT COUNT(1) FROM trends_good WHERE trends_id = t.trends_id AND good_deleted = 1) AS goodCount," +
            "(SELECT COUNT(1) FROM trends_comment WHERE trends_id = t.trends_id AND comment_deleted = 1) AS commentCount," +
            "t.trends_photo AS trendsPhoto FROM" +
            "`trends` t INNER JOIN usermessage u ON t.user_id = u.user_id inner join cim_img_file c " +
            "on c.id = u.user_header WHERE t.trends_deleted = 1 AND t.trends_id = #{trendsId} ORDER BY t.trends_createtime DESC")
    Trends selectTrendByTrendsIdNotLogin(@Param("trendsId")Integer trendsId);


    /**
     * 登录后根据动态id查询动态信息
     *
     * @param trendsId 趋势id
     * @param userId   用户id
     * @return {@link Trends}
     */
    @Select("SELECT t.trends_id AS trendsId," +
            "t.user_id AS userId," +
            "u.user_account AS userAccount," +
            "u.user_name AS userName," +
            "u.user_header AS userHeader," +
            "u.user_address AS userAddress," +
            "c.file_path AS filePath," +
            "t.trends_title AS trendsTitle," +
            "t.trends_createtime AS trendsCreatetime," +
            "t.trends_content AS trendsContent," +
            "(SELECT COUNT(1) FROM trends_good WHERE trends_id = t.trends_id AND good_deleted = 1) AS goodCount," +
            "(SELECT COUNT(1) FROM trends_comment WHERE trends_id = t.trends_id AND comment_deleted = 1) AS commentCount," +
            "(SELECT COUNT(1) FROM trends_good WHERE user_id = #{userId} AND trends_id = t.trends_id AND good_deleted = 1) AS goodRed," +
            "t.trends_photo AS trendsPhoto FROM" +
            "`trends` t INNER JOIN usermessage u ON t.user_id = u.user_id inner join cim_img_file c " +
            "on c.id = u.user_header WHERE t.trends_deleted = 1 AND t.trends_id = #{trendsId} ORDER BY t.trends_createtime DESC")
    Trends selectTrendByTrendsId(@Param("userId")Integer userId,@Param("trendsId")Integer trendsId);

}

