package com.pethome.util;

/**
 * @author Mr.Wang
 */
public class ResultEntity {
	/**
	 * 编码
	 */
	private Integer code;

	/**
	 * 数据
	 */
	private Object data;

	/**
	 * 信息
	 */
	private String msg;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ResultEntity(Integer code, Object data, String msg) {
		this.code = code;
		this.data = data;
		this.msg = msg;
	}

	/**
	 * 返回对象
	 * @param data
	 */
	public ResultEntity(Object data) {
		this.code = 200;
		this.data = data;
		this.msg = "成功!";
	}
}
