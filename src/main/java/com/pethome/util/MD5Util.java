package com.pethome.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

/**
 * @desc md5工具
 * @author 张淮鑫
 */
@Slf4j
public class MD5Util {

	/**
	 * @desc 获取文件md5
	 * @param fileInputStream 文件流
	 * @return md5
	 */
	public static String file(InputStream fileInputStream) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] buffer = new byte[8192];
			int length = 0;
			while ((length = fileInputStream.read(buffer)) != -1) {
				md5.update(buffer, 0, length);
			}
			return new String(Hex.encodeHex(md5.digest()));
		} catch (Exception e) {
			log.error("file{} MD5 fail", e);
			return null;
		} finally {
			try {
				if (null != fileInputStream) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				log.error("close fileInputStream fail", e);
				return null;
			}
		}
	}

	/**
	 * @desc 获取文件md5
	 * @param file 文件
	 * @return md5
	 */
	public static String file(File file) {
		InputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] buffer = new byte[8192];
			int length = 0;
			while ((length = fileInputStream.read(buffer)) != -1) {
				md5.update(buffer, 0, length);
			}
			return new String(Hex.encodeHex(md5.digest()));
		} catch (Exception e) {
			log.error("file{} MD5 fail", e);
			return null;
		} finally {
			try {
				if (null != fileInputStream) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				log.error("close fileInputStream fail", e);
				return null;
			}
		}
	}

}
