package com.pethome.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/**
 * @desc crc32工具
 * @author 张淮鑫
 */
@Slf4j
public class CRC32Util {

	/**
	 * @desc 获取文件crc32
	 * @param fileInputStream 文件流
	 * @return crc32
	 */
	public static Long file(InputStream fileInputStream) {
		try {
			CheckedInputStream cis = new CheckedInputStream(fileInputStream, new CRC32());
			byte[] buf = new byte[128];
			int read = cis.read(buf);
			while (read >= 0) {
				read = cis.read(buf);
			}
			long checksum = cis.getChecksum().getValue();
			return checksum;
		} catch (Exception e) {
			log.error("file{} CRC32 fail", e);
			return null;
		} finally {
			try {
				if (null != fileInputStream) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				log.error("close fileInputStream fail", e);
				return null;
			}
		}

	}

	/**
	 * @desc 获取文件crc32
	 * @param file 文件
	 * @return crc32
	 */
	public static Long file(File file) {
		InputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);
			CheckedInputStream cis = new CheckedInputStream(fileInputStream, new CRC32());
			byte[] buf = new byte[128];
			int read = cis.read(buf);
			while (read >= 0) {
				read = cis.read(buf);
			}
			long checksum = cis.getChecksum().getValue();
			return checksum;
		} catch (Exception e) {
			log.error("file{} CRC32 fail", e);
			return null;
		} finally {
			try {
				if (null != fileInputStream) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				log.error("close fileInputStream fail", e);
				return null;
			}
		}

	}

}
