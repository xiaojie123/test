/**
 * 
 */
package com.pethome.util;

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * @author 34307
 * 发送短信验证码
 */
public class MessageUtil {
	private static String apiUrl = "http://106.ihuyi.com/webservice/sms.php?method=Submit";
    private static String appId = "C16491427";
    private static String appSecret = "d6f5acb8a7220461dff2f1a60b77ba9c";

    /**
     * 发送验证码
     * @param mobile
     * @param randomCode
     * @return
     */
    public static ResultEntity sendMessage(String mobile,Integer randomCode){
    	   try {
    	    URL url = new URL(apiUrl);
    	    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    	    connection.setDoOutput(true);//允许连接提交信息
    	    connection.setRequestMethod("POST");//网页提交方式“GET”、“POST”
    	    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    	    connection.setRequestProperty("Connection", "Keep-Alive");
    	    StringBuffer sb = new StringBuffer();
    	    sb.append("account="+appId);
    	    sb.append("&password="+appSecret);
    	    sb.append("&mobile="+mobile);
    	    StringBuffer cont = new StringBuffer();
    	    cont.append("您的登录验证码是：");
    	    cont.append(randomCode);
    	    cont.append("。请在规定时间内登录，请不要把验证码泄漏给其他人");
    	    sb.append("&content="+cont);
    	    sb.append("&format=json");
    	    java.io.OutputStream os =  connection.getOutputStream();
    	    os.write(sb.toString().getBytes());
    	    os.close();

    	    String line, result = "";
    	    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(),"utf-8"));
    	    while ((line = in.readLine()) != null) {
    	     result += line + "\n";
    	    }
    	    in.close();
            JSONObject jsonObject = JSONObject.parseObject(result);
            jsonObject.put("randomCode",String.valueOf(randomCode));
            if("2".equals(jsonObject.get("code").toString())) {
            	return new ResultEntity(200,jsonObject, "发送成功！");
            }else {
            	return new ResultEntity(0, null, "提交失败!");
            }
    	   } catch (Exception e) {
	    	    e.printStackTrace(System.out);
	    	    return new ResultEntity(0, null, "提交失败!");
    	   }
    }
    
    /**
     * 随机验证码
     * @return
     */
    public static int randomUtil() {
    	return (new Random()).nextInt(100000)+1;
    }
    
}
