package com.pethome.util;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.*;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @desc 文件工具栏
 * @author 廖玮琦
 */
public class FileUtil {

	/**
	 * @desc 获取文件后缀
	 * @param fileName 文件名
	 * @return 后缀信息
	 */
	public static String getSuffix(String fileName) {
		int dotIdx = fileName.lastIndexOf('.');
		return (dotIdx == -1) ? "" : fileName.toLowerCase().substring(dotIdx + 1);
	}

	/**
	 * @desc 获取文件大小 MB
	 * @param file 文件名
	 * @return 后缀信息
	 */
	public static double getFileSize(File file) {
		if (file != null) {
			return getMb(file.length());
		}
		return 0;
	}

	public static double getFileSizeKb(File file) {
		if (file != null) {
			return getKb(file.length());
		}
		return 0;
	}

	public static String randomString(Integer e) {
		String t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
		Integer a = t.length();
		String n = "";
		for (int i = 0; i < e; i++) {
			n += t.charAt((int) Math.floor(Math.random() * a));
		}
		return n;
	}

	public static String randomString(Integer e, String t) {
		Integer a = t.length();
		String n = "";
		for (int i = 0; i < e; i++) {
			n += t.charAt((int) Math.floor(Math.random() * a));
		}
		return n;
	}

	public static double getMb(double size) {
		double fileSizeMB = new BigDecimal(size / 1024 / 1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return fileSizeMB;
	}

	public static double getKb(double size) {
		double fileSizeMB = new BigDecimal(size / 1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return fileSizeMB;
	}

	/**
	 * @desc 获取完整文件后缀
	 * @param fileName 文件名
	 * @return 后缀信息
	 */
	public static String getCompleteSuffix(String fileName) {
		int dotIdx = fileName.lastIndexOf('.');
		return (dotIdx == -1) ? "" : fileName.substring(dotIdx);
	}

	/**
	 * @desc 获取文件名称
	 * @param fileName 文件名
	 * @return 文件名
	 */
	public static String getFileName(String fileName) {
		int dotIdx = fileName.lastIndexOf('.');
		return (dotIdx == -1) ? "" : fileName.substring(0, dotIdx);
	}

	/**
	 * @desc 获取url完整文件名称
	 * @param url
	 * @return
	 */
	public static String getUrlCompleteFileName(String url) {
		int dotIdx = url.lastIndexOf('/');
		return (dotIdx == -1) ? "" : url.substring(dotIdx + 1);
	}

	/**
	 * @desc 判断文件是否图片
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notImg(String fileName) {
		return !isImg(fileName);
	}

	/**
	 * @desc 判断文件是否图片
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isImg(String fileName) {
		return isSuffix(fileName, ".+(.JPEG|.jpeg|.JPG|.jpg|.png|.PNG|.gif|.GIF)$");
	}

	/**
	 * @desc 判断文件是否为文档
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notDoc(String fileName) {
		return !isDoc(fileName);
	}

	/**
	 * @desc 判断文件是否文档
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isDoc(String fileName) {
		return isSuffix(fileName,
				".+(.DOC|.doc|.DOCX|.docx|.XLS|.xls|.XLSX|.xlsx|.PPT|.ppt|.PPTX|.pptx|.txt|.TXT|.PDF|.pdf)$");
	}

	/**
	 * @desc 判断文件是否WORD
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isWord(String fileName) {
		return isSuffix(fileName, ".+(.DOC|.doc|.DOCX|.docx|.txt|.TXT)$");
	}

	/**
	 * @desc 判断文件是否PDF
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isPdf(String fileName) {
		return isSuffix(fileName, ".+(.PDF|.pdf)$");
	}

	/**
	 * @desc 判断文件是否PPT
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isPpt(String fileName) {
		return isSuffix(fileName, ".+(.PPT|.ppt|.PPTX|.pptx)$");
	}

	/**
	 * @desc 判断文件是否为视频
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notVideo(String fileName) {
		return !isVideo(fileName);
	}

	/**
	 * @desc 判断文件是否视频
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isVideo(String fileName) {
		return isSuffix(fileName, ".+(.MP4|.mp4|.AVI|.avi|.WMV|.wmv)$");
	}

	/**
	 * @desc 判断文件是否附件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notAnnex(String fileName) {
		return !isAnnex(fileName);
	}

	/**
	 * @desc 判断文件是否附件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isAnnex(String fileName) {
		return isSuffix(fileName,
				".+(.PDF|.pdf|.ZIP|.zip|.RAR|.rar|.7Z|.7z|.DOC|.doc|.DOCX|.docx|.XLS|.xls|.XLSX|.xlsx)$");
	}

	/**
	 * @desc 判断文件是否glb
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notGlb(String fileName) {
		return !isGlb(fileName);
	}

	/**
	 * @desc 判断文件是否glb
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isGlb(String fileName) {
		return isSuffix(fileName, ".+(.GLB|.glb)$");
	}

	/**
	 * @desc 判断文件是否dwg
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notDwg(String fileName) {
		return !isDwg(fileName);
	}

	/**
	 * @desc 判断文件是否dwg
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isDwg(String fileName) {
		return isSuffix(fileName, ".+(.DWG|.dwg)$");
	}

	/**
	 * @desc 判断文件是否zip
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notZip(String fileName) {
		return !isZip(fileName);
	}

	/**
	 * @desc 判断文件是否zip
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isZip(String fileName) {
		return isSuffix(fileName, ".+(.ZIP|.zip)$");
	}

	/**
	 * @desc 判断文件是否
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notSvf(String fileName) {
		return !isSvf(fileName);
	}

	/**
	 * @desc 判断文件是否
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isSvf(String fileName) {
		return isSuffix(fileName, ".+(.SVF|.svf)$");
	}

	/**
	 * @desc 判断文件是否rvt
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notRvt(String fileName) {
		return !isRvt(fileName);
	}

	/**
	 * @desc 判断文件是否rvt
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isRvt(String fileName) {
		return isSuffix(fileName, ".+(.RVT|.rvt)$");
	}

	public static boolean isRvtOrDwg(String fileName) {
		return isSuffix(fileName, ".+(.RVT|.rvt|.DWG|.dwg)$");
	}

	/**
	 * @desc 判断文件是否model文件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notModel(String fileName) {
		return !isModel(fileName);
	}

	/**
	 * @desc 判断文件是否model文件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isModel(String fileName) {
		return isSuffix(fileName, ".+(.GLB|.glb|.RVT|.rvt)$");
	}

	/**
	 * @desc 判断文件是否gis
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notGis(String fileName) {
		return !isGis(fileName);
	}

	/**
	 * @desc 判断文件是否gis
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isGis(String fileName) {
		return isSuffix(fileName, ".+(.GEOJSON|.geojson|.JPEG|.jpeg|.JPG|.jpg|.PNG|.png|.RVT|.rvt|.ZIP|.zip)$");
	}

	/**
	 * @desc 判断文件是否为指定静态文件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notStaticFile(String fileName) {
		return !isImg(fileName);
	}

	/**
	 * @desc 判断文件是否指定静态文件
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isStaticFile(String fileName) {
		return isSuffix(fileName, ".+(.CSV|.scv|.XLS|.xls|.XLSX|.xlsx)$");
	}

	/**
	 * @desc 判断文件是否png或jpg
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notPngOrJpg(String fileName) {
		return !isPngOrJpg(fileName);
	}

	/**
	 * @desc 判断文件是否png或jpg
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isPngOrJpg(String fileName) {
		return isSuffix(fileName, ".+(.JPEG|.jpeg|.JPG|.jpg|.PNG|.png)$");
	}

	/**
	 * @desc 判断文件是否geojson
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notGeojson(String fileName) {
		return !isGeojson(fileName);
	}

	/**
	 * @desc 判断文件是否geojson
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isGeojson(String fileName) {
		return isSuffix(fileName, ".+(.GEOJSON|.geojson)$");
	}

	/**
	 * @desc 判断文件是否json
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notJson(String fileName) {
		return !isJson(fileName);
	}

	/**
	 * @desc 判断文件是否json
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isJson(String fileName) {
		return isSuffix(fileName, ".+(.JSON|.json)$");
	}

	/**
	 * @desc 判断文件是否tileset.json
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notTileset(String fileName) {
		return !isTileset(fileName);
	}

	/**
	 * @desc 判断文件是否tileset.json
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isTileset(String fileName) {
		return isSuffix(fileName, ".+(tileset.json)$");
	}

	/**
	 * @desc 判断文件是否csv
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notCsv(String fileName) {
		return !isCsv(fileName);
	}

	/**
	 * @desc 判断文件是否csv
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isCsv(String fileName) {
		return isSuffix(fileName, ".+(.CSV|.csv)$");
	}

	/**
	 * @desc 判断文件是否xls
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notXls(String fileName) {
		return !isXls(fileName);
	}

	/**
	 * @desc 判断文件是否xls
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isXls(String fileName) {
		return isSuffix(fileName, ".+(.XLS|.xls)$");
	}

	/**
	 * @desc 判断文件是否xlsx
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notXlsx(String fileName) {
		return !isXlsx(fileName);
	}

	/**
	 * @desc 判断文件是否xlsx
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isXlsx(String fileName) {
		return isSuffix(fileName, ".+(.XLSX|.xlsx)$");
	}

	/**
	 * @desc 判断文件是否excel
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean notExcel(String fileName) {
		return !isExcel(fileName);
	}

	/**
	 * @desc 判断文件是否excel
	 * @param fileName 文件名
	 * @return boolean
	 */
	public static boolean isExcel(String fileName) {
		return isSuffix(fileName, ".+(.XLS|.xls|.XLSX|.xlsx)$");
	}

	/**
	 * @desc 判断文件是否包含指定后缀
	 * @param fileName 文件名
	 * @param reg      指定后缀（正则）
	 * @return
	 */
	public static boolean isSuffix(String fileName, String reg) {
		Matcher matcher = Pattern.compile(reg).matcher(fileName);
		return matcher.find();
	}

	/**
	 * @desc 获取文件夹
	 * @return 文件夹
	 */
	public static String getFolder() {
		char[] c = new char[2];
		c[0] = (char) ((int) (Math.random() * 25) + 65);
		c[1] = (char) ((int) (Math.random() * 25) + 65);
		return String.valueOf(c) + "/";
	}

	/**
	 * @desc 获取文件夹
	 * @param level 层级
	 * @return 文件夹
	 */
	public static String getFolder(int level) {
		level = level < 1 ? 1 : level;
		String folders = "";
		for (int i = 0; i < level; i++) {
			folders += getFolder();
		}
		return folders;
	}

	/**
	 * @desc 随机存取文件
	 * @param in   输入文件
	 * @param out  输出文件
	 * @param seek 偏移量
	 * @throws IOException
	 */
	public static void randomAccessFile(File in, File out, Long seek) throws IOException {
		RandomAccessFile raFile = null;
		BufferedInputStream inputStream = null;
		try {
			// 以读写的方式打开目标文件
			raFile = new RandomAccessFile(out, "rw");
			raFile.seek(seek);
			inputStream = new BufferedInputStream(new FileInputStream(in));
			byte[] buf = new byte[1024];
			int length = 0;
			while ((length = inputStream.read(buf)) != -1) {
				raFile.write(buf, 0, length);
			}
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (raFile != null) {
					raFile.close();
				}
			} catch (Exception e) {
				throw new IOException(e.getMessage());
			}
		}
	}

	/**
	 * @desc 删除单个文件
	 * @param sPath 被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String sPath) {
		boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * @desc 删除目录（文件夹）以及目录下的文件
	 * @param sPath 被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String sPath) {
		// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
		if (!sPath.endsWith(File.separator)) {
			sPath = sPath + "/";
		}
		File dirFile = new File(sPath);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		boolean flag = true;
		// 删除文件夹下的所有文件(包括子目录)
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 删除子文件
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag) {
					break;
				}
			} // 删除子目录
			else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag) {
					break;
				}
			}
		}
		if (!flag) {
			return false;
		}
		// 删除当前目录
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	public static void downloadFile(String url, String filePath) {
		File file = new File(filePath);
		if (file.length() == 0) {
			try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
				HttpGet httpGet = new HttpGet(url);
				try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
					HttpEntity entity = httpResponse.getEntity();
					if (entity != null) {
						FileUtils.copyInputStreamToFile(entity.getContent(), file);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
