package com.pethome.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pethome.entity.CimImgFile;

import java.util.List;

/**
 * cim图片文件(CimImgFile)表服务接口
 *
 * @author makejava
 * @since 2023-05-23 09:33:45
 */
public interface CimImgFileService extends IService<CimImgFile> {

    List<CimImgFile> getImgFileByCrc32(long crc32);

    Integer insertUploadImage(CimImgFile cimImgFile);

}

