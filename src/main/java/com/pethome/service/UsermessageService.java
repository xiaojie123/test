package com.pethome.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pethome.entity.Usermessage;

/**
 * (Usermessage)表服务接口
 *
 * @author makejava
 * @since 2023-04-25 14:25:45
 */
public interface UsermessageService extends IService<Usermessage> {


    /**
     * 用户登录(账号密码)
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    Integer userLoginForAccount(Usermessage usermessage);

    /**
     * 用户登录(手机号密码)
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    Integer userLoginForPhone(Usermessage usermessage);

    /**
     * 选择计算帐户
     *
     * @param userAccount 用户帐户
     * @return {@link Integer}
     */
    Integer selectCountAccount(String userAccount);

    /**
     * regiter用户
     *
     * @param usermessage usermessage
     * @return {@link Integer}
     */
    Integer regiterUser(Usermessage usermessage);


    /**
     * 更新用户密码
     *
     * @param userPassword 用户密码
     * @param userPhone    用户电话
     * @return {@link Integer}
     */
    Integer updateUserPassword(String userPassword,String userPhone);

    /**
     * 选择用户帐户信息
     *
     * @param userAccount 用户帐户
     * @return {@link Usermessage}
     */
    Usermessage selectUserMessageByAccount(String userAccount);

    /**
     * 选择用户信息通过电话
     *
     * @param userPhone 用户电话
     * @return {@link Usermessage}
     */
    Usermessage selectUserMessageByPhone(String userPhone);

    /**
     * 查找粉丝数
     *
     * @param userId 用户id
     * @return {@link Integer}
     */
    Integer getFansCount(Integer userId);


    /**
     * 互相关注人的数量
     *
     * @param userId 用户id
     * @return {@link Integer}
     */
    Integer getFollowFansCount(Integer userId);

}

