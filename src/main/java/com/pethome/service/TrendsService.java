package com.pethome.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pethome.entity.Trends;

import java.util.List;

/**
 * (Trends)表服务接口
 *
 * @author makejava
 * @since 2023-05-21 19:50:53
 */
public interface TrendsService extends IService<Trends> {

    List<Trends> selectAllTrendsMessage();

    List<Trends> selectAllTrendsMessageByUserId(Integer userId);

    List<Trends> selectHotTrend();

    Integer selectGoodTrends(Integer trendsId,Integer userId);

    Integer InsertGoodTrends(Integer trendsId,Integer userId);

    Integer removeGoodTrends(Integer trendsId,Integer userId);

    Trends selectTrendByTrendsIdNotLogin(Integer trendsId);

    Trends selectTrendByTrendsId(Integer userId,Integer trendsId);

}

