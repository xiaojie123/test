package com.pethome.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pethome.dao.UsermessageDao;
import com.pethome.entity.Usermessage;
import com.pethome.service.UsermessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * (Usermessage)表服务实现类
 *
 * @author makejava
 * @since 2023-04-25 14:25:45
 */
@Service("usermessageService")
public class UsermessageServiceImpl extends ServiceImpl<UsermessageDao, Usermessage> implements UsermessageService {

    @Autowired(required = false)
    UsermessageDao usermessageDao;

    @Override
    public Integer userLoginForAccount(Usermessage usermessage) {
        return usermessageDao.userLoginForAccount(usermessage);
    }

    @Override
    public Integer userLoginForPhone(Usermessage usermessage) {
        return usermessageDao.userLoginForPhone(usermessage);
    }

    @Override
    public Integer selectCountAccount(String userAccount) {
        return usermessageDao.selectCountAccount(userAccount);
    }

    @Override
    public Integer regiterUser(Usermessage usermessage) {
        return usermessageDao.regiterUser(usermessage);
    }

    @Override
    public Integer updateUserPassword(String userPassword, String userPhone) {
        return usermessageDao.updateUserPassword(userPassword,userPhone);
    }

    @Override
    public Usermessage selectUserMessageByAccount(String userAccount) {
        return usermessageDao.selectUserMessageByAccount(userAccount);
    }

    @Override
    public Usermessage selectUserMessageByPhone(String userPhone) {
        return usermessageDao.selectUserMessageByPhone(userPhone);
    }

    @Override
    public Integer getFansCount(Integer userId) {
        return usermessageDao.getFansCount(userId);
    }

    @Override
    public Integer getFollowFansCount(Integer userId) {
        return usermessageDao.getFollowFansCount(userId);
    }
}

