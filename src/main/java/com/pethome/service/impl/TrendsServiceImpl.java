package com.pethome.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pethome.dao.TrendsDao;
import com.pethome.entity.Trends;
import com.pethome.service.TrendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (Trends)表服务实现类
 *
 * @author makejava
 * @since 2023-05-21 19:50:54
 */
@Service("trendsService")
public class TrendsServiceImpl extends ServiceImpl<TrendsDao, Trends> implements TrendsService {

    @Autowired(required = false)
    TrendsDao trendsDao;

    @Override
    public List<Trends> selectAllTrendsMessage() {
        return trendsDao.selectAllTrendsMessage();
    }

    @Override
    public List<Trends> selectAllTrendsMessageByUserId(Integer userId) {
        return trendsDao.selectAllTrendsMessageByUserId(userId);
    }

    @Override
    public List<Trends> selectHotTrend() {
        return trendsDao.selectHotTrend();
    }

    @Override
    public Integer selectGoodTrends(Integer trendsId, Integer userId) {
        return trendsDao.selectGoodTrends(trendsId, userId);
    }

    @Override
    public Integer InsertGoodTrends(Integer trendsId, Integer userId) {
        return trendsDao.InsertGoodTrends(trendsId, userId);
    }

    @Override
    public Integer removeGoodTrends(Integer trendsId, Integer userId) {
        return trendsDao.removeGoodTrends(trendsId, userId);
    }

    @Override
    public Trends selectTrendByTrendsIdNotLogin(Integer trendsId) {
        return trendsDao.selectTrendByTrendsIdNotLogin(trendsId);
    }

    @Override
    public Trends selectTrendByTrendsId(Integer userId, Integer trendsId) {
        return trendsDao.selectTrendByTrendsId(userId,trendsId);
    }

}

