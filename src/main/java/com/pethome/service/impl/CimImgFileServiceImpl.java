package com.pethome.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pethome.dao.CimImgFileDao;
import com.pethome.entity.CimImgFile;
import com.pethome.service.CimImgFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * cim图片文件(CimImgFile)表服务实现类
 *
 * @author makejava
 * @since 2023-05-23 09:33:46
 */
@Service("cimImgFileService")
public class CimImgFileServiceImpl extends ServiceImpl<CimImgFileDao, CimImgFile> implements CimImgFileService {

    @Resource
    private CimImgFileDao cimImgFileDao;

    @Override
    public List<CimImgFile> getImgFileByCrc32(long crc32) {
        return this.cimImgFileDao.getImgFileByCrc32(crc32);
    }

    @Override
    public Integer insertUploadImage(CimImgFile cimImgFile) {
        return this.cimImgFileDao.insertUploadImage(cimImgFile);
    }
}

