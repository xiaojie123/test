package com.pethome.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "spring.tencent")
public class OssConfig {

    private String accesskey;
    private String secretKey;
    private String bucket;
    private String bucketName;
    private String path;
    private String preffix;

}
