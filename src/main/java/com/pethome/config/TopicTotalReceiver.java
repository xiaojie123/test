package com.pethome.config;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;

import java.util.Map;

/**
 * @Author : JCccc
 * @CreateTime : 2019/9/3
 * @Description :
 **/

//@Component
//@RabbitListener(queues = "topic.woman")
public class TopicTotalReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("woman消费者收到消息  : " + testMessage.toString());
    }
}
