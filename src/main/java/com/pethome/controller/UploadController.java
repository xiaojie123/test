package com.pethome.controller;

import com.pethome.entity.CimImgFile;
import com.pethome.service.CimImgFileService;
import com.pethome.util.CRC32Util;
import com.pethome.util.FtpUtil;
import com.pethome.util.MD5Util;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = "/upload")
public class UploadController {

    /**
     * 服务对象
     */
    @Resource
    private CimImgFileService cimImgFileService;

    @Value("${ftp.imgUrl}")
    String imgUrl;

    /**
     * @param file 文件
     * @return ResultBean
     * @throws IOException
     * @desc 上传图片
     */
    @ResponseBody
    @PostMapping("/uploadTrends")
    public Object uploadTrends(@NotNull MultipartFile[] file) throws IOException {
        String result = "";
        for (MultipartFile files : file) {
            // 校验文件CRC32和MD5
            Long crc32 = CRC32Util.file(files.getInputStream());
            List<CimImgFile> list = cimImgFileService.getImgFileByCrc32(crc32);
            String md5 = MD5Util.file(files.getInputStream());
            if (list.size() > 0) {
                for (CimImgFile imgFile : list) {
                    if (md5.equals(imgFile.getMd5())) {
                        return new UploadMsg(200, "上传成功", imgFile.getFilePath());
                    }
                }
            } else {
                try {
                    // 获取原始上传的文件名
                    String oldFileName = files.getOriginalFilename();
                    // 获取文件类型[后缀]0
                    String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
                    // 使用UUID生成新的文件名
                    String newFileName = UUID.randomUUID() + suffix;

                    // 2、把图片上传到图片服务器
                    // 2.1获取上传的io流
                    InputStream input = null;
                    try {
                        input = files.getInputStream();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (suffix.equals(".mp4")) {
                        result = FtpUtil.putVedio(input, newFileName);
                    }
                    if (suffix.equals(".png") || suffix.equals(".jpg")) {
                        result = FtpUtil.putImages(input, newFileName);
                    }


                    CimImgFile imgFile = new CimImgFile();
                    imgFile.setFileName(oldFileName);
                    imgFile.setSuffix(suffix);
                    imgFile.setFileSize(files.getSize());
                    imgFile.setFilePath(imgUrl + newFileName);
                    imgFile.setCrc32(crc32);
                    imgFile.setMd5(md5);
                    imgFile.setCreateTime(new Date());

                    Integer res = cimImgFileService.insertUploadImage(imgFile);

                    if(res>0){
                        return new UploadMsg(200, "上传成功", result);
                    }else{
                        return new UploadMsg(250, "上传失败", null);
                    }

                } catch (Exception e) {
                    return new UploadMsg(250, e.getMessage(), null);
                }
            }
        }
        return new UploadMsg(200, "上传完毕", null);
    }

    /**
     * @param file 文件
     * @return ResultBean
     * @throws IOException
     * @desc 上传图片
     */
//    @ResponseBody
//    @PostMapping("/uploadImg")
//    public Object uploadImg(@NotNull MultipartFile[] file) throws IOException {
//        for (MultipartFile files : file) {
//            // 校验文件CRC32和MD5
//            Long crc32 = CRC32Util.file(files.getInputStream());
//            List<CimImgFile> list = cimImgFileService.getImgFileByCrc32(crc32);
//            String md5 = MD5Util.file(files.getInputStream());
//            if (list.size() > 0) {
//                for (CimImgFile imgFile : list) {
//                    if (md5.equals(imgFile.getMd5())) {
//                        return new UploadMsg(200, "上传成功", imgFile.getFilePath());
//                    }
//                }
//            } else {
//                System.out.println("1");
//                // 获取原始上传的文件名
//                String oldFileName = files.getOriginalFilename();
//                // 获取文件类型[后缀]0
//                String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
//                // 使用UUID生成新的文件名
//                String newFileName = UUID.randomUUID() + suffix;
//                // 1 初始化用户身份信息(secretId, secretKey)
//                COSCredentials cred = new BasicCOSCredentials(ossConfig.getAccesskey(), ossConfig.getSecretKey());
//                // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//                ClientConfig clientConfig = new ClientConfig(new Region(ossConfig.getBucket()));
//                // 3 生成cos客户端
//                COSClient cosclient = new COSClient(cred, clientConfig);
//                // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//                String bucketName = ossConfig.getBucketName();
//
//                // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
//                // 大文件上传请参照 API 文档高级 API 上传
//                File localFile = null;
//                try {
//                    localFile = File.createTempFile("temp", null);
//                    files.transferTo(localFile);
//                    // 指定要上传到 COS 上的路径
//                    String key = null;
//                    if (suffix.equals(".mp4")) {
//                        System.out.println("2");
//                        key = "/video/" + newFileName;
//                    }
//                    if (suffix.equals(".mp3")) {
//                        key = "/audio/" + newFileName;
//                    }
//                    if (suffix.equals(".png") || suffix.equals(".jpg")) {
//                        key = "/" + ossConfig.getPreffix() + "/" + oldFileName;
//                    }
//                    // 上传文件的请求对象
//                    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
//                    // 使用COS客户端上传文件
//                    PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
//
//                    CimImgFile imgFile = new CimImgFile();
//                    imgFile.setFileName(oldFileName);
//                    imgFile.setSuffix(suffix);
//                    imgFile.setFileSize(files.getSize());
//                    imgFile.setFilePath(ossConfig.getPath() + putObjectRequest.getKey());
//                    imgFile.setCrc32(crc32);
//                    imgFile.setMd5(md5);
//                    imgFile.setCreateTime(new Date());
//
//                    Integer res = cimImgFileService.insertUploadImage(imgFile);
//
//                    if(res>0){
//                        return new UploadMsg(200, "上传成功", ossConfig.getPath() + putObjectRequest.getKey());
//                    }else{
//                        return new UploadMsg(250, "上传失败", null);
//                    }
//
//                } catch (IOException e) {
//                    return new UploadMsg(250, e.getMessage(), null);
//                } finally {
//                    // 关闭客户端(关闭后台线程)
//                    cosclient.shutdown();
//                }
//            }
//        }
//        return new UploadMsg(200, "上传完毕", null);
//    }

    /**
     * 上传到腾讯云服务器（https://cloud.tencent.com/document/product/436/10199）
     *
     * @return
     */
//    @PostMapping(value = "/tencent")
//    @ResponseBody
//    public Object upload(@RequestParam(value = "file") MultipartFile file) {
//        if (file == null) {
//            return new UploadMsg(0, "文件为空", null);
//        }
//        // 获取原始上传的文件名
//        String oldFileName = file.getOriginalFilename();
//        // 获取文件类型[后缀]
//        String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
//
//        // 使用UUID生成新的文件名
////        String newFileName = UUID.randomUUID() + suffix;
//        // 上传文件按日期生成的路径
////        Calendar cal = Calendar.getInstance();
////        int year = cal.get(Calendar.YEAR);
////        int month = cal.get(Calendar.MONTH);
////        int day = cal.get(Calendar.DATE);
//        // 1 初始化用户身份信息(secretId, secretKey)
//        COSCredentials cred = new BasicCOSCredentials(ossConfig.getAccesskey(), ossConfig.getSecretKey());
//        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//        ClientConfig clientConfig = new ClientConfig(new Region(ossConfig.getBucket()));
//        // 3 生成cos客户端
//        COSClient cosclient = new COSClient(cred, clientConfig);
//        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//        String bucketName = ossConfig.getBucketName();
//
//        // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
//        // 大文件上传请参照 API 文档高级 API 上传
//        File localFile = null;
//        try {
//            localFile = File.createTempFile("temp", null);
//            file.transferTo(localFile);
//            // 指定要上传到 COS 上的路径
//            String key = null;
//            if (suffix.equals(".mp4")) {
//                key = "/video/" + oldFileName;
//            }
//            if (suffix.equals(".mp3")) {
//                key = "/audio/" + oldFileName;
//            }
//            if (suffix.equals(".png") || suffix.equals(".jpg")) {
//                key = "/" + ossConfig.getPreffix() + "/" + oldFileName;
//            }
//            // 上传文件的请求对象
//            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
//            // 使用COS客户端上传文件
//            PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
//            return new UploadMsg(1, "上传成功", ossConfig.getPath() + putObjectRequest.getKey());
//        } catch (IOException e) {
//            return new UploadMsg(-1, e.getMessage(), null);
//        } finally {
//            // 关闭客户端(关闭后台线程)
//            cosclient.shutdown();
//        }
//    }


//    @PostMapping(value = "/uploadshop")
//    @ResponseBody
//    public Object uploadshop(@RequestParam(value = "file") MultipartFile[] file) {
//
//        for (int i = 0; i <= file.length; i++) {
//            // 获取原始上传的文件名
//            String oldFileName = file[i].getOriginalFilename();
//            // 获取文件类型[后缀]0
//            String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
//
//            // 使用UUID生成新的文件名
////        String newFileName = UUID.randomUUID() + suffix;
//            // 上传文件按日期生成的路径
////        Calendar cal = Calendar.getInstance();
////        int year = cal.get(Calendar.YEAR);
////        int month = cal.get(Calendar.MONTH);
////        int day = cal.get(Calendar.DATE);
//            // 1 初始化用户身份信息(secretId, secretKey)
//            COSCredentials cred = new BasicCOSCredentials(ossConfig.getAccesskey(), ossConfig.getSecretKey());
//            // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//            ClientConfig clientConfig = new ClientConfig(new Region(ossConfig.getBucket()));
//            // 3 生成cos客户端
//            COSClient cosclient = new COSClient(cred, clientConfig);
//            // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//            String bucketName = ossConfig.getBucketName();
//
//            // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
//            // 大文件上传请参照 API 文档高级 API 上传
//            File localFile = null;
//            try {
//                localFile = File.createTempFile("temp", null);
//                file[i].transferTo(localFile);
//                // 指定要上传到 COS 上的路径
//                String key = null;
//                if (suffix.equals(".mp4")) {
//                    key = "/video/" + oldFileName;
//                }
//                if (suffix.equals(".mp3")) {
//                    key = "/audio/" + oldFileName;
//                }
//                if (suffix.equals(".png") || suffix.equals(".jpg")) {
//                    key = "/image/shop/" + oldFileName;
//                }
//                // 上传文件的请求对象
//                PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
//                // 使用COS客户端上传文件
//                PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
//                return new UploadMsg(1, "上传成功", ossConfig.getPath() + putObjectRequest.getKey());
//            } catch (IOException e) {
//                return new UploadMsg(-1, e.getMessage(), null);
//            } finally {
//                // 关闭客户端(关闭后台线程)
//                cosclient.shutdown();
//            }
//        }
//        return new UploadMsg(1, "上传完毕", null);
//    }
//
//    @PostMapping(value = "/uploadtrends")
//    @ResponseBody
//    public Object uploadtrends(@RequestParam(value = "file") MultipartFile file) {
//        // 获取原始上传的文件名
//        String oldFileName = file.getOriginalFilename();
//        // 获取文件类型[后缀]
//        String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
//
//        // 使用UUID生成新的文件名
////        String newFileName = UUID.randomUUID() + suffix;
//        // 上传文件按日期生成的路径
////        Calendar cal = Calendar.getInstance();
////        int year = cal.get(Calendar.YEAR);
////        int month = cal.get(Calendar.MONTH);
////        int day = cal.get(Calendar.DATE);
//        // 1 初始化用户身份信息(secretId, secretKey)
//        COSCredentials cred = new BasicCOSCredentials(ossConfig.getAccesskey(), ossConfig.getSecretKey());
//        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//        ClientConfig clientConfig = new ClientConfig(new Region(ossConfig.getBucket()));
//        // 3 生成cos客户端
//        COSClient cosclient = new COSClient(cred, clientConfig);
//        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//        String bucketName = ossConfig.getBucketName();
//
//        // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
//        // 大文件上传请参照 API 文档高级 API 上传
//        File localFile = null;
//        try {
//            localFile = File.createTempFile("temp", null);
//            file.transferTo(localFile);
//            // 指定要上传到 COS 上的路径
//            String key = null;
//            if (suffix.equals(".mp4")) {
//                key = "/video/" + oldFileName;
//            }
//            if (suffix.equals(".mp3")) {
//                key = "/audio/" + oldFileName;
//            }
//            if (suffix.equals(".png") || suffix.equals(".jpg")) {
//                key = "/image/trends/" + oldFileName;
//            }
//            // 上传文件的请求对象
//            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
//            // 使用COS客户端上传文件
//            PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);
//            return new UploadMsg(1, "上传成功", ossConfig.getPath() + putObjectRequest.getKey());
//        } catch (IOException e) {
//            return new UploadMsg(-1, e.getMessage(), null);
//        } finally {
//            // 关闭客户端(关闭后台线程)
//            cosclient.shutdown();
//        }
//    }

    private class UploadMsg {
        public int status;
        public String msg;
        public String path;

        public UploadMsg() {
            super();
        }

        public UploadMsg(int status, String msg, String path) {
            this.status = status;
            this.msg = msg;
            this.path = path;
        }
    }

}
