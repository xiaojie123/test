package com.pethome.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.pethome.entity.Trends;
import com.pethome.service.TrendsService;
import com.pethome.util.ResultEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Trends)表控制层
 *
 * @author makejava
 * @since 2023-05-21 19:50:52
 */
@RestController
@RequestMapping("trends")
public class TrendsController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private TrendsService trendsService;

    /**
     * 查询所有动态信息
     *
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/selectAllTrendsMessage")
    public ResultEntity selectAllTrendsMessage() {

        Map<String, Object> map = new HashMap<String, Object>();

        List<Trends> trends = trendsService.selectAllTrendsMessage();

        map.put("trends",trends);

        return new ResultEntity(map);

    }

    /**
     * 查询所有动态信息
     *
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/selectAllTrendsMessageByUserId")
    public ResultEntity selectAllTrendsMessageByUserId(@RequestParam("userId")Integer userId) {

        Map<String, Object> map = new HashMap<String, Object>();

        List<Trends> trends = trendsService.selectAllTrendsMessageByUserId(userId);

        map.put("trends",trends);

        return new ResultEntity(map);

    }

    /**
     * 查询热门动态
     *
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/selectHotTrend")
    public ResultEntity selectHotTrend() {

        Map<String, Object> map = new HashMap<String, Object>();

        List<Trends> trendsHost = trendsService.selectHotTrend();

        map.put("trendsHost",trendsHost);

        return new ResultEntity(map);

    }

    /**
     * 判断有没有点赞
     *
     * @param trendsId 趋势id
     * @param userId   用户id
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/selectGoodTrends")
    public ResultEntity selectGoodTrends(@RequestParam("trendsId")Integer trendsId,@RequestParam("userId")Integer userId) {

        Map<String, Object> map = new HashMap<String, Object>();

        if(userId == null || userId == 0){
            return new ResultEntity(250,map,"请登录再点赞！");
        }

        Integer res = trendsService.selectGoodTrends(trendsId,userId);

        map.put("res",res);

        return new ResultEntity(map);

    }

    /**
     * 新增点赞
     *
     * @param trendsId 趋势id
     * @param userId   用户id
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/InsertGoodTrends")
    public ResultEntity InsertGoodTrends(@RequestParam("trendsId")Integer trendsId,@RequestParam("userId")Integer userId) {

        Map<String, Object> map = new HashMap<String, Object>();

        if(userId == null || userId == 0){
            return new ResultEntity(250,map,"请登录再点赞！");
        }

        Integer res = trendsService.InsertGoodTrends(trendsId,userId);

        map.put("res",res);

        return new ResultEntity(map);

    }

    /**
     * 取消点赞
     *
     * @param trendsId 趋势id
     * @param userId   用户id
     * @return {@link ResultEntity}
     */
    @GetMapping(value = "/removeGoodTrends")
    public ResultEntity removeGoodTrends(@RequestParam("trendsId")Integer trendsId,@RequestParam("userId")Integer userId) {

        Map<String, Object> map = new HashMap<String, Object>();

        if(userId == null || userId == 0){
            return new ResultEntity(250,map,"请登录再点赞！");
        }

        Integer res = trendsService.removeGoodTrends(trendsId,userId);

        map.put("res",res);

        return new ResultEntity(map);

    }

    @GetMapping(value = "/selectTrendByTrendsId")
    public ResultEntity selectTrendByTrendsId(@RequestParam("trendsId")Integer trendsId,@RequestParam("userId")Integer userId) {

        Map<String, Object> map = new HashMap<String, Object>();

        Integer res = trendsService.removeGoodTrends(userId,trendsId);

        map.put("res",res);

        return new ResultEntity(map);

    }

}

