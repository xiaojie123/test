package com.pethome.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import com.pethome.auth.SnowFlakeIdWorker;
import com.pethome.entity.Usermessage;
import com.pethome.service.UsermessageService;
import com.pethome.util.*;
import com.wf.captcha.ArithmeticCaptcha;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * (Usermessage)表控制层
 *
 * @author makejava
 * @since 2023-04-25 14:25:44
 */
@RestController
@RequestMapping("usermessage")
public class UsermessageController extends ApiController {

    /**
     * 服务对象
     */
    @Resource
    private UsermessageService usermessageService;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 用户登录账户(账号密码)
     *
     * @param account  账户
     * @param password 密码
     * @return {@link ResultEntity}
     */
    @PostMapping("/userLoginForAccount")
    public ResultEntity userLoginForAccount(@RequestParam(value = "account", required = true) String account,
                                            @RequestParam(value = "password", required = true) String password,
                                            @RequestParam(value = "code", required = true) String code,
                                            HttpSession session) {

        Map<String, Object> map = new HashMap<String, Object>();

        if (account == null || account == "") {
            return new ResultEntity(250, map, "账号或者手机号错误");
        }

        if (password == null || password == "") {
            return new ResultEntity(250, map, "密码错误");
        }

        if (code == null || code == "") {
            return new ResultEntity(270, map, "验证码错误");
        }

        String validateCode = (String) session.getAttribute("validate_code");

        if (validateCode.equals(code)) {
            Usermessage usermessage = new Usermessage();
            usermessage.setUserAccount(account);
            usermessage.setUserPassword(DigestUtils.md5Hex(password));

            Integer res = usermessageService.userLoginForAccount(usermessage);

            if (res > 0) {

                Usermessage usermessage1 = usermessageService.selectUserMessageByAccount(account);

                map.put("userId", usermessage1.getUserId());
                map.put("userAccount", usermessage1.getUserAccount());
                map.put("userName", usermessage1.getUserName());
                map.put("userPassword", usermessage1.getUserPassword());
                map.put("userSex", usermessage1.getUserSex());
                map.put("userBirthday", usermessage1.getUserBirthday());
                map.put("userHeader", usermessage1.getUserHeader());
                map.put("userIntroduction", usermessage1.getUserIntroduction());
                map.put("userAddress", usermessage1.getUserAddress());
                map.put("userPhone", usermessage1.getUserPhone());
                map.put("userEmail", usermessage1.getUserEmail());
                map.put("filePath", usermessage1.getFilePath());
                map.put("userRole", usermessage1.getUserRole());

                SnowFlakeIdWorker idWorker = new SnowFlakeIdWorker(1, 1);
                String tokenId = Long.toString(idWorker.nextId());
                tokenId = EncryptionUtil.encrypt(tokenId);
                // 设置过期失效1小时
                redisUtil.set(tokenId, map, 3600);
                // 返回前端 密码置空
                usermessage.setUserPassword(null);
                Map<String, Object> userMap = BeanToMapUtil.beanToMap(map);
                userMap.put("accessToken", tokenId);
                return new ResultEntity(userMap);
            } else {
                map.put("res", res);

                return new ResultEntity(250, map, "账号密码输入错误！");
            }
        }

        return new ResultEntity(270, map, "验证码错误！");

    }

    /**
     * 发送短信验证码
     *
     * @param phone
     * @param session
     * @return
     */
    @GetMapping(value = "/sendMsg/{phone}")
    public Object sendMsg(@PathVariable(value = "phone", required = true) String phone, HttpSession session) {
        if (phone.length() != 11) {
            return new ResultEntity(250, null, "手机格式不正确!");
        }

        //判断数据库中是否有手机号
        Usermessage usermessage = new Usermessage();
        usermessage.setUserPhone(phone);
        Integer res = usermessageService.userLoginForPhone(usermessage);

        if (res <= 0) {
            return new ResultEntity(250, null, "没有注册此手机号!");
        }

        //验证码
        Integer randomCode = MessageUtil.randomUtil();

        System.out.println("========>" + randomCode);

        ResultEntity re = MessageUtil.sendMessage(phone, randomCode);

        System.out.println("========>" + re.getCode());

        if (re.getCode() == 200) {
            session.setAttribute("randomCode", randomCode.toString());
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    if (session.getAttribute("randomCode") != null) {
                        session.removeAttribute("randomCode");
                    }
                }
            };
            Timer timer = new Timer();
            //设置session 十分钟后结束
            timer.schedule(task, 600000);
        }
        return re;
    }

    /**
     * getsession
     * 获取验证码
     *
     * @param session 会话
     * @return {@link ResultEntity}
     */
    @GetMapping("/getsession")
    public ResultEntity getsession(HttpSession session) {
        Map<String, Object> map = new HashMap<String, Object>();

        Integer code = Integer.valueOf(session.getAttribute("randomCode").toString());
        System.out.println(code);

        map.put("code", code);

        return new ResultEntity(map);
    }

    /**
     * 用户登录手机
     *
     * @param phone      电话
     * @param yanzenCode yanzen代码
     * @return {@link ResultEntity}
     */
    @GetMapping("/userLoginForPhone")
    public ResultEntity userLoginForPhone(@RequestParam(value = "phone", required = true) String phone,
                                          @RequestParam(value = "yanzenCode") String yanzenCode,
                                          HttpSession session) {

        Map<String, Object> map = new HashMap<String, Object>();

        String Code = session.getAttribute("randomCode").toString();

        if (!Code.equals(yanzenCode)) {
            return new ResultEntity(250, map, "验证码错误");
        }

        if (phone == null || phone == "") {
            return new ResultEntity(250, map, "账号错误");
        }

        Usermessage usermessage = new Usermessage();
        usermessage.setUserPhone(phone);

        Integer res = usermessageService.userLoginForPhone(usermessage);

        if (res <= 0) {
            return new ResultEntity(250, null, "没有注册此手机号!");
        }

        Usermessage usermessage1 = usermessageService.selectUserMessageByAccount(phone);

        map.put("userId", usermessage1.getUserId());
        map.put("userAccount", usermessage1.getUserAccount());
        map.put("userName", usermessage1.getUserName());
        map.put("userPassword", usermessage1.getUserPassword());
        map.put("userSex", usermessage1.getUserSex());
        map.put("userBirthday", usermessage1.getUserBirthday());
        map.put("userHeader", usermessage1.getUserHeader());
        map.put("filePath", usermessage1.getFilePath());
        map.put("userIntroduction", usermessage1.getUserIntroduction());
        map.put("userAddress", usermessage1.getUserAddress());
        map.put("userPhone", usermessage1.getUserPhone());
        map.put("userEmail", usermessage1.getUserEmail());
        map.put("userRole", usermessage1.getUserRole());

        SnowFlakeIdWorker idWorker = new SnowFlakeIdWorker(1, 1);
        String tokenId = Long.toString(idWorker.nextId());
        tokenId = EncryptionUtil.encrypt(tokenId);
        // 设置过期失效1小时
        redisUtil.set(tokenId, map, 3600);
        // 返回前端 密码置空
        usermessage.setUserPassword(null);
        Map<String, Object> userMap = BeanToMapUtil.beanToMap(map);
        userMap.put("accessToken", tokenId);

        return new ResultEntity(userMap);

    }

    /**
     * 验证手机
     *
     * @param phone      电话
     * @param yanzenCode yanzen代码
     * @param session    会话
     * @return {@link ResultEntity}
     */
    @GetMapping("/verifyPhone")
    public ResultEntity verifyPhone(@RequestParam(value = "phone", required = true) String phone,
                                    @RequestParam(value = "yanzenCode") String yanzenCode,
                                    HttpSession session) {

        Map<String, Object> map = new HashMap<String, Object>();

        String Code = session.getAttribute("randomCode").toString();

        if (!Code.equals(yanzenCode)) {
            return new ResultEntity(250, map, "验证码错误");
        }

        if (phone == null || phone == "") {
            return new ResultEntity(250, map, "账号错误");
        }

        Usermessage usermessage = new Usermessage();
        usermessage.setUserPhone(phone);

        Integer res = usermessageService.userLoginForPhone(usermessage);

        if (res <= 0) {
            return new ResultEntity(250, null, "没有注册此手机号!");
        }

        map.put("res", res);

        return new ResultEntity(map);

    }


    /**
     * 获取图形验证码
     *
     * @param response 响应
     * @param session  会话
     * @throws IOException ioexception
     */
    @GetMapping("/captcha/{id}")
    public void getCaptcha(HttpServletResponse response, HttpSession session) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        // 算术验证码
        ArithmeticCaptcha arithmeticCaptcha = new ArithmeticCaptcha(120, 40);
        String code = arithmeticCaptcha.text();
        System.out.println("您的验证码是：" + code);
        session.setAttribute("validate_code", code);
        arithmeticCaptcha.out(outputStream);

        // 中文验证码
//        ChineseCaptcha captcha =new ChineseCaptcha(120, 40);
//        String code = captcha.text();
//        System.out.println("您的验证码是：" + code);
//        session.setAttribute("validate_code", code);
//        captcha.out(outputStream);

        // 英文与数字验证码
//        SpecCaptcha captcha = new SpecCaptcha(120, 40);
//        String code = captcha.text();
//        System.out.println("您的验证码是：" + code);
//        session.setAttribute("validate_code", code);
//        captcha.out(outputStream);

        // 英文与数字动态验证码
        // GifCaptcha captcha = new GifCaptcha(120, 40);

        // 中文动态验证码
//        ChineseGifCaptcha chineseCaptcha = new ChineseGifCaptcha();
//        chineseCaptcha.setLen(2);

    }


    /**
     * 查询有没有此帐户
     *
     * @param account 账户
     * @return {@link ResultEntity}
     */
    @GetMapping("/selectCountAccount")
    public ResultEntity selectCountAccount(@RequestParam(value = "account", required = true) String account) {
        Map<String, Object> map = new HashMap<String, Object>();

        Integer countAccount = usermessageService.selectCountAccount(account);

        if (countAccount > 0) {
            return new ResultEntity(250, map, "此账号已被使用");
        }

        map.put("countAccount", countAccount);

        return new ResultEntity(map);

    }

    /**
     * 选择统计电话
     *
     * @param phone 电话
     * @return {@link ResultEntity}
     */
    @GetMapping("/selectCountPhone/{phone}")
    public ResultEntity selectCountPhone(@PathVariable(value = "phone", required = true) String phone) {

        Map<String, Object> map = new HashMap<String, Object>();
        Usermessage usermessage = new Usermessage();
        usermessage.setUserPhone(phone);

        Integer res = usermessageService.userLoginForPhone(usermessage);

        if (res > 0) {
            return new ResultEntity(250, map, "一个手机号只能绑定一个账号，请更换手机号");
        }

        map.put("res", res);

        return new ResultEntity(map);

    }

    /**
     * 注册发送短信验证码
     *
     * @param phone
     * @param session
     * @return
     */
    @GetMapping(value = "/sendMsgRegits/{phone}")
    public Object sendMsgRegits(@PathVariable(value = "phone", required = true) String phone, HttpSession session) {
        if (phone.length() != 11) {
            return new ResultEntity(250, null, "手机格式不正确!");
        }

        //验证码
        Integer randomCode = MessageUtil.randomUtil();

        ResultEntity re = MessageUtil.sendMessage(phone, randomCode);

        if (re.getCode() == 200) {
            session.setAttribute("randomCode", randomCode.toString());
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    if (session.getAttribute("randomCode") != null) {
                        session.removeAttribute("randomCode");
                    }
                }
            };
            Timer timer = new Timer();
            //设置session 十分钟后结束
            timer.schedule(task, 600000);
        }
        return re;
    }

    /**
     * regiter用户
     *
     * @param account    账户
     * @param username   用户名
     * @param password   密码
     * @param phone      电话
     * @param yanzenCode yanzen代码
     * @param session    会话
     * @return {@link ResultEntity}
     */
    @PostMapping("/regiterUser")
    public ResultEntity regiterUser(@RequestParam(value = "account", required = true) String account,
                                    @RequestParam(value = "username", required = true) String username,
                                    @RequestParam(value = "password", required = true) String password,
                                    @RequestParam(value = "phone", required = true) String phone,
                                    @RequestParam(value = "yanzenCode", required = true) String yanzenCode,
                                    HttpSession session) {

        Map<String, Object> map = new HashMap<String, Object>();

        Integer countAccount = usermessageService.selectCountAccount(account);

        if (countAccount > 0) {
            return new ResultEntity(250, map, "此账号已被使用");
        }

        Usermessage usermessage = new Usermessage();
        usermessage.setUserAccount(account);
        usermessage.setUserName(username);
        usermessage.setUserPassword(DigestUtils.md5Hex(password));
        usermessage.setUserPhone(phone);

        Integer res = usermessageService.userLoginForPhone(usermessage);

        if (res > 0) {
            return new ResultEntity(250, map, "一个手机号只能绑定一个账号，请更换手机号");
        }

        String Code = session.getAttribute("randomCode").toString();

        if (!Code.equals(yanzenCode)) {
            return new ResultEntity(250, map, "验证码错误");
        }

        Integer count = usermessageService.regiterUser(usermessage);

        if (count <= 0) {
            return new ResultEntity(250, map, "注册失败！");
        }

        map.put("count", count);

        return new ResultEntity(map);

    }


    /**
     * 更新密码
     *
     * @param phone    电话
     * @param password 密码
     * @param session  会话
     * @return {@link ResultEntity}
     */
    @GetMapping("/updatePassword")
    public ResultEntity updatePassword(@RequestParam(value = "phone", required = true) String phone,
                                       @RequestParam(value = "password", required = true) String password,
                                       HttpSession session) {

        Map<String, Object> map = new HashMap<String, Object>();

        Integer count = usermessageService.updateUserPassword(DigestUtils.md5Hex(password), phone);

        if (count <= 0) {
            return new ResultEntity(250, map, "修改失败！");
        }

        map.put("count", count);

        return new ResultEntity(map);

    }

    /**
     * 获得token
     *
     * @param req 要求事情
     * @return {@link Object}
     */
    @GetMapping("/getTokenMess")
    public Object getTokenMess(HttpServletRequest req) {
        //开始获取token
        String token = req.getHeader("token");
        //如果Header没有token
        if (Tools.isEmpty(token)) {
            token = req.getParameter("token");
            //如果参数里面有没有Token
            if (Tools.isEmpty(token)) {
                return "请求里面没有";
            }
        }
        System.out.println(redisUtil.get(token));
        return redisUtil.get(token);
    }

    @GetMapping("/removeTokenMess")
    public ResultEntity removeTokenMess() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                    .getRequest();
            String token = request.getAttribute("accessToken").toString();
            redisUtil.del(token);
            return new ResultEntity(null);
        } catch (Exception e) {
            return new ResultEntity(250, null, "退出失败！");
        }
    }

    /**
     * 获得粉丝数
     *
     * @return {@link ResultEntity}
     */
    @GetMapping("/getFansCount")
    public ResultEntity getFansCount(@RequestParam("userId")Integer userId) {

        Integer FansCount = usermessageService.getFansCount(userId);

        return new ResultEntity(FansCount);

    }

    /**
     * 得到关注数量
     *
     * @param userId 用户id
     * @return {@link ResultEntity}
     */
    @GetMapping("/getFollowFansCount")
    public ResultEntity getFollowFansCount(@RequestParam("userId")Integer userId) {

        Integer FollowFansCount = usermessageService.getFollowFansCount(userId);

        return new ResultEntity(FollowFansCount);

    }

}

