package com.pethome.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * (Usermessage)表实体类
 *
 * @author makejava
 * @since 2023-04-25 14:25:44
 */
public class Usermessage extends Model<Usermessage> {
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 用户帐户
     */
    private String userAccount;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户密码
     */
    private String userPassword;
    /**
     * 用户性别
     */
    private Integer userSex;
    /**
     * 用户生日
     */
    private Date userBirthday;
    /**
     * 用户头像(默认)
     */
    private Integer userHeader;
    /**
     * 用户签名
     */
    private String userIntroduction;
    /**
     * 用户地址
     */
    private String userAddress;
    /**
     * 用户电话
     */
    private String userPhone;
    /**
     * 用户电子邮件
     */
    private String userEmail;
    /**
     * 用户角色
     */
    private Integer userRole;

    /**
     * 用户粉丝数量
     */
    private Integer fansCount;

    /**
     * 互相关注数量
     */
    private Integer followFansCount;

    /**
     * 用户是否删除(默认)
     */
    private Integer userDeleted;

    /**
     * 文件路径
     */
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getFollowFansCount() {
        return followFansCount;
    }

    public void setFollowFansCount(Integer followFansCount) {
        this.followFansCount = followFansCount;
    }

    public Integer getFansCount() {
        return fansCount;
    }

    public void setFansCount(Integer fansCount) {
        this.fansCount = fansCount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Integer getUserSex() {
        return userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Integer getUserHeader() {
        return userHeader;
    }

    public void setUserHeader(Integer userHeader) {
        this.userHeader = userHeader;
    }

    public String getUserIntroduction() {
        return userIntroduction;
    }

    public void setUserIntroduction(String userIntroduction) {
        this.userIntroduction = userIntroduction;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Integer getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        this.userRole = userRole;
    }

    public Integer getUserDeleted() {
        return userDeleted;
    }

    public void setUserDeleted(Integer userDeleted) {
        this.userDeleted = userDeleted;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.userId;
    }
}

