package com.pethome.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * (Trends)表实体类
 *
 * @author makejava
 * @since 2023-05-21 19:50:53
 */
@SuppressWarnings("serial")
public class Trends extends Model<Trends> {
    //动态id
    private Integer trendsId;
    //动态作者
    private Integer userId;
    /**
     * 用户帐户
     */
    private String userAccount;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户头像(默认)
     */
    private Integer userHeader;
    private String userAddress;
    //动态标题
    private String trendsTitle;
    //动态发布时间
    private Date trendsCreatetime;
    //动态内容
    private String trendsContent;
    //点赞数量
    private Integer goodCount;
    //评论数量
    private Integer commentCount;
    //动态图片
    private String trendsPhoto;
    //动态删除
    private Integer trendsDeleted;
    /**
     * 文件路径
     */
    private String filePath;

    private Integer goodRed;

    public Integer getGoodRed() {
        return goodRed;
    }

    public void setGoodRed(Integer goodRed) {
        this.goodRed = goodRed;
    }

    public Integer getGoodCount() {
        return goodCount;
    }

    public void setGoodCount(Integer goodCount) {
        this.goodCount = goodCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserHeader() {
        return userHeader;
    }

    public void setUserHeader(Integer userHeader) {
        this.userHeader = userHeader;
    }

    public Integer getTrendsId() {
        return trendsId;
    }

    public void setTrendsId(Integer trendsId) {
        this.trendsId = trendsId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTrendsTitle() {
        return trendsTitle;
    }

    public void setTrendsTitle(String trendsTitle) {
        this.trendsTitle = trendsTitle;
    }

    public Date getTrendsCreatetime() {
        return trendsCreatetime;
    }

    public void setTrendsCreatetime(Date trendsCreatetime) {
        this.trendsCreatetime = trendsCreatetime;
    }

    public String getTrendsContent() {
        return trendsContent;
    }

    public void setTrendsContent(String trendsContent) {
        this.trendsContent = trendsContent;
    }

    public String getTrendsPhoto() {
        return trendsPhoto;
    }

    public void setTrendsPhoto(String trendsPhoto) {
        this.trendsPhoto = trendsPhoto;
    }

    public Integer getTrendsDeleted() {
        return trendsDeleted;
    }

    public void setTrendsDeleted(Integer trendsDeleted) {
        this.trendsDeleted = trendsDeleted;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.trendsId;
    }
}

