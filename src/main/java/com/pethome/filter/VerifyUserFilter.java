/**  

* <p>Title: VerifyUserFilter.java</p>  

* <p>Description: </p>  

* <p>Copyright: Copyright (c) 2017</p>  

* <p>Company: http://www.teoform.com</p>   

* @date 2018年10月15日  

* @version 1.0  

*/
package com.pethome.filter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.pethome.util.CookieUtil;
import com.pethome.util.RedisUtil;
import com.pethome.util.ResultEntity;
import com.pethome.util.Tools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 
 * <p>
 * Title: VerifyUserFilter
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * @author ls
 * 
 * @date 2018年10月15日
 * 
 * 
 */
@WebFilter(filterName = "VerifyUserFilter", urlPatterns = "/*")
public class VerifyUserFilter implements Filter {

	private static final String ACCESSTOKEN = "accessToken";
	@Autowired
	private RedisUtil redisUtil;
	// 不需要过滤就可以访问的路径(比如:注册登录等)
	protected static String[] authorizedUrls = new String[] { "/usermessage/**","/static/**"};

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("text/javascript");
		// 是否需要过滤
		boolean needFilter = isNeedFilter(req);
		if (!needFilter) { // 不需要过滤直接传给下一个过滤器
			chain.doFilter(req, resp);
			return;
		}
		// 开始获取token
		// 1.header中获取
		String token = req.getHeader(ACCESSTOKEN);
		// 如果在cookie中没找到token
		if (Tools.isEmpty(token)) {
			// 2. 地址栏获取
			token = req.getParameter(ACCESSTOKEN);
			if (Tools.isEmpty(token)) {
				// 3.cookie中获取
				token = findCookieIsExistToken(req, ACCESSTOKEN);
				if (Tools.isEmpty(token)) {
					// 三种方式都尝试获取token 如果还是获取不到那么就认为该请求无效
					String returnStr = JSON.toJSONString(new ResultEntity(250,null,"用户未登录"));
					resp.getWriter().write(returnStr);
					return;
				}
			}
		}
		// 根据token到redis中获取用户信息
		Object tmpUser = redisUtil.get(token);
		if (null == tmpUser) {
			String returnStr = JSON.toJSONString(new ResultEntity(250,null,"登录已失效"));
			resp.getWriter().write(returnStr);
			return;
		}
		// System.out.println("redis中获取token成功：token--->"+token);
		@SuppressWarnings("unchecked")
		Map<String, Object> userinfo = (Map<String, Object>) tmpUser;
		if (userinfo.isEmpty() || null == userinfo.get("cache_userId").toString()) {
			String returnStr = JSON.toJSONString(new ResultEntity(250,null,"token错误"));
			resp.getWriter().write(returnStr);
			return;
		}
		//添加用户id到请求域中
		request.setAttribute("userId", userinfo.get("cache_userId"));
		request.setAttribute("isAdmin", userinfo.get("cache_isAdmin"));
		request.setAttribute("accessToken", token);
		redisUtil.expire(token, 3600);
		chain.doFilter(req, resp);
	}

	/**
	 * <p>
	 * Title: findCookieIsExistToken
	 * </p>
	 * <p>
	 * Description: cookie中找寻 accessToken字段
	 * </p>
	 * 
	 * @param request
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private String findCookieIsExistToken(HttpServletRequest request, String queryParam)
			throws JsonParseException, JsonMappingException, IOException {
		// cookie 中获取令牌
		Cookie cookie = CookieUtil.getCookieByName(request, queryParam);
		// 说明没有在cookie中获取到token 直接返回
		if (cookie == null) {
			return null;
		}
		// 获取值
		String token = cookie.getValue();
		if (Tools.isEmpty(token)) {
			return null;
		}
		return token;
	}

	/**
	 * 
	 * <p>
	 * Title: isNeedFilter
	 * </p>
	 * 
	 * <p>
	 * Description: 是否需要过滤
	 * </p>
	 *
	 *            要过滤的uri
	 * @return false不过滤 true 过滤
	 * 
	 */
	public boolean isNeedFilter(HttpServletRequest req) {
		for (String includeUrl : authorizedUrls) {
			PathMatcher matcher = new AntPathMatcher();
			if (matcher.match(includeUrl, req.getRequestURI())) {
				return false;
			}
		}
		return true;
	}
}